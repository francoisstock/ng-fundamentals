import { CommonModule } from '@angular/common';
import { NgModule, Optional, SkipSelf } from '@angular/core';

import { throwIfAlreadyLoaded } from './module-import-check';
import { IToastr, JQUERY_TOKEN, TOASTR_TOKEN } from './services';

// tslint:disable-next-line: no-string-literal
const toastr: IToastr = window['toastr'];
// tslint:disable-next-line: no-string-literal
const jquery = window['$'];

@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  providers: [
    { provide: JQUERY_TOKEN, useValue: jquery },
    { provide: TOASTR_TOKEN, useValue: toastr }
  ]
})
export class CoreModule {
  public constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
    throwIfAlreadyLoaded(parentModule, 'CoreModule');
  }
 }
