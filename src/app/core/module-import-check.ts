export function throwIfAlreadyLoaded(parentModule: any, moduleName: string): void {
    if (parentModule) {
        const message = `${parentModule} has already been loaded. Import this module in AppModule only`;
        throw new Error(message);
    }
}
