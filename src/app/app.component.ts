import { Component, OnInit } from '@angular/core';
import { AuthService } from './user/auth.service';

@Component({
  selector: 'buza-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  public constructor(private readonly authService: AuthService) { /**/ }

  public ngOnInit(): void {
    this.authService.checkAuthenticationStatus();
  }
}
