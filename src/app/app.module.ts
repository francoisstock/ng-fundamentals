import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import {
  CreateEventComponent,
  CreateSessionComponent,
  DurationPipe,
  EventDetailComponent,
  EventListComponent,
  // EventRouteActivatorService,
  EventListResolverService,
  EventResolverService,
  EventService,
  EventThumbnailComponent,
  LocationValidatorDirective,
  SessionListComponent,
  VoterService
} from './events';

import { APP_ROUTES } from './app.routes';
import { CoreModule } from './core/core.module';
import { UpvoteComponent } from './events/upvote/upvote.component';
import { LayoutModule } from './layout';
import { SharedModule } from './shared/shared.module';
import { AuthService } from './user/auth.service';

@NgModule({
  declarations: [
    AppComponent,

    CreateEventComponent,
    CreateSessionComponent,
    EventDetailComponent,
    EventListComponent,
    EventThumbnailComponent,
    SessionListComponent,

    DurationPipe,

    UpvoteComponent,

    LocationValidatorDirective
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(APP_ROUTES),

    CoreModule,
    LayoutModule,
    SharedModule
  ],
  providers: [
    AuthService,
    EventListResolverService,
    // EventRouteActivatorService,
    EventResolverService,
    EventService,
    VoterService,

    { provide: 'canDeactivateCreateEvent', useValue: checkDirtyState },
    // { provide: MinimalLogger, useExisting: Logger } // can reduce interface to really used function
    // { provide: Logger, useFactory: loggerFactory() }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

export function checkDirtyState(component: CreateEventComponent) {
  if (component.isDirty) {
    return window.confirm('You have not saved this event, do you really want to cancel?');
  }

  return true;
}
