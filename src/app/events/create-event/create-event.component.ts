import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { EventService, IEvent } from '../shared';

@Component({
  selector: 'buza-create-event',
  templateUrl: './create-event.component.html',
  styleUrls: ['./create-event.component.scss']
})
export class CreateEventComponent {
  public isDirty = true;
  public readonly newEvent: IEvent;

  public constructor(
    private readonly service: EventService,
    private readonly router: Router
  ) { /**/ }

  public cancel(): void {
    this.redirect();
  }

  public save(formValues): void {
    this.service.create(formValues).subscribe(() => {
      this.isDirty = false;
      this.redirect();
    });
  }

  private redirect(): void {
    this.router.navigate(['/events']);
  }
}
