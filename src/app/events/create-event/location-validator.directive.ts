import { Directive } from '@angular/core';
import { FormGroup, NG_VALIDATORS, Validator } from '@angular/forms';

@Directive({
  selector: '[buzaLocationValidator]',
  providers: [{
    provide: NG_VALIDATORS,
    useExisting: LocationValidatorDirective,
    multi: true  // prevent service to be singleton
  }]
})
export class LocationValidatorDirective implements Validator {
  private static isAddressValid(formGroup: FormGroup): boolean {
    const addressControl = formGroup.controls.address;
    const cityControl = formGroup.controls.city;
    const countryControl = formGroup.controls.country;

    return !!addressControl && !!addressControl.value &&
      !!cityControl && !!cityControl.value &&
      !!countryControl && !!countryControl.value;
  }

  private static isUrlValid(formGroup: FormGroup): boolean {
    return !!formGroup && !!formGroup.value && !!formGroup.valid;
  }

  private static isAnyValid(formGroup): boolean {
    const formRoot = formGroup.root as FormGroup;
    const onlineUrlControl = formRoot && formRoot.controls.onlineUrl as FormGroup;

    return LocationValidatorDirective.isAddressValid(formGroup) ||
      LocationValidatorDirective.isUrlValid(onlineUrlControl);
  }

  public validate(formGroup: FormGroup): { [key: string]: any } {
    return LocationValidatorDirective.isAnyValid(formGroup) ?
      null :
      { validateLocation: false };
  }
}
