import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'buza-upvote',
  templateUrl: './upvote.component.html',
  styleUrls: ['./upvote.component.scss']
})
export class UpvoteComponent {
  @Input() public count: number;
  @Input() public hasVoted: boolean;
  @Output() public vote = new EventEmitter();

  public get iconColor(): string {
    return this.hasVoted ? 'red' : 'white';
  }

  public onClick(): void {
    this.vote.emit({});
  }
}
