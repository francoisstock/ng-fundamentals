// Not used anymore but demonstrate usage of route activator

// import { Injectable } from '@angular/core';
// import { CanActivate, ActivatedRouteSnapshot, Router } from '@angular/router';

// import { EventService } from '../shared/event.service';

// @Injectable()
// export class EventRouteActivatorService implements CanActivate {
//   constructor(private readonly service: EventService, private readonly router: Router) { }

//   public canActivate(route: ActivatedRouteSnapshot): boolean {
//     const id = +route.params.id;
//     const event = this.service.getOne(id);
//     const eventExists = !!event;

//     if (!eventExists) {
//       this.router.navigate(['/404']);
//     }

//     return eventExists;
//   }
// }
