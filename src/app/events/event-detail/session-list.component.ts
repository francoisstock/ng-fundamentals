import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { AuthService } from 'src/app/user/auth.service';
import { ISession, VoterService } from '../shared';

@Component({
  selector: 'buza-session-list',
  templateUrl: './session-list.component.html',
  styleUrls: ['./session-list.component.scss']
})
export class SessionListComponent implements OnChanges {

  private get isAllFilter() { return this.filterBy === 'all'; }
  @Input() public eventId: number;
  @Input() public sessions: ISession[];
  @Input() public filterBy: string;
  @Input() public sortBy: string;
  public filteredSesson: ISession[];

  private get userName(): string {
    const currentUser = this.authService.currentUser;
    return currentUser && currentUser.userName;
  }

  public constructor(
    public readonly authService: AuthService,
    private readonly voterService: VoterService
  ) { /**/ }

  private static sortByName(left: ISession, right: ISession): number {
    return left.name.localeCompare(right.name);
  }

  private static sortByVotesDesc(left: ISession, right: ISession): number {
    return right.voters.length - left.voters.length;
  }

  public ngOnChanges() {
    if (this.sessions) {
      this.filterSessions(this.filterBy);

      const sortMethod = this.sortBy === 'name' ?
        SessionListComponent.sortByName :
        SessionListComponent.sortByVotesDesc;
      this.filteredSesson.sort(sortMethod);
    }
  }

  private filterSessions(filter: string) {
    this.filteredSesson = this.isAllFilter ?
      this.sessions.slice(0)
      : this.sessions.filter(
        (session) => session.level.toLowerCase() === this.filterBy.toLowerCase()
      );
  }

  public toggleVote(session: ISession): void {
    if (this.hasUserVoted(session)) {
      this.voterService.deleteVoter(this.eventId, session, this.userName);
    } else {
      this.voterService.addVoter(this.eventId, session, this.userName);
    }

    if (this.sortBy === 'votes') {
      this.filteredSesson.sort(SessionListComponent.sortByVotesDesc);
    }
  }

  public hasUserVoted(session: ISession): boolean {
    return this.voterService.hasUserVoted(session, this.userName);
  }
}
