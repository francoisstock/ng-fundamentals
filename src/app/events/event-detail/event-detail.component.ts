import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';

import { EventService, IEvent, ISession } from '../shared';

@Component({
  selector: 'buza-event-detail',
  templateUrl: './event-detail.component.html',
  styleUrls: ['./event-detail.component.scss']
})
export class EventDetailComponent implements OnInit {
  public event: IEvent;
  public addMode = false;
  public filterBy = 'all';
  public sortBy = 'votes';

  public constructor(
    private readonly eventService: EventService,
    private readonly route: ActivatedRoute
  ) { /**/ }

  public ngOnInit(): void {
    this.route.data.forEach((data) => {
      this.event = this.route.snapshot.data.event;
      this.addMode = false;
    });
  }

  public addSession(): void {
    this.addMode = true;
  }

  public onSessionSaved(session: ISession): void {
    const ids = this.event.sessions.map((s) => s.id);
    const nextId = Math.max.apply(null, ids);
    session.id = nextId + 1;

    this.event.sessions.push(session);
    this.eventService.update(this.event).subscribe(() => {
      this.addMode = false;
    });
  }

  public onSessionsCanceled(): void {
    this.addMode = false;
  }
}
