import { DebugElement, NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

import { AuthService } from '../../user/auth.service';
import { DurationPipe, ISession, VoterService } from '../shared';
import { SessionListComponent } from './session-list.component';

describe('SessionListComponent', () => {
  let fixture: ComponentFixture<SessionListComponent>;
  let component: SessionListComponent;
  let element: HTMLElement;
  let debugElement: DebugElement;

  beforeEach(async(() => {
    const authServiceMock = {
      currentUser: { userName: 'joe' }
    };
    const voterServiceMock = {
      hasUserVoted: () => true
    };

    TestBed.configureTestingModule({
      imports: [],
      declarations: [
        DurationPipe,
        SessionListComponent,

        // NO_ERRORS_SCHEMA allow shallow integrated tests
        // Angular will no longer complains about unknown components
        // Use with caution!

        // CollapsibleWellComponent,
        // UpvoteComponent
      ],
      providers: [{
        provide: AuthService, useValue: authServiceMock
      }, {
        provide: VoterService, useValue: voterServiceMock
      }],
      schemas: [
        NO_ERRORS_SCHEMA
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SessionListComponent);
    component = fixture.componentInstance;
    element = fixture.nativeElement;
    debugElement = fixture.debugElement;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('initial display', () => {
    it('should show session title', () => {
      const session: ISession = {
        id: 3, name: 'Session 1', presenter: 'Joe', duration: 1,
        level: 'beginner', abstract: 'any abstract', voters: []
      };
      component.sessions = [session];
      component.filterBy = 'all';
      component.sortBy = 'name';
      component.eventId = 4;

      // Not automaticly triggered in test!
      component.ngOnChanges();
      fixture.detectChanges();

      // Two methods for same result
      const expectedTitle = 'Session 1';
      expect(element.querySelector('[well-title]').textContent).toContain(expectedTitle);
      expect(debugElement.query(By.css('[well-title]')).nativeElement.textContent).toContain(expectedTitle);
    });
  });
});
