import { AuthService } from 'src/app/user/auth.service';
import { ISession, VoterService } from '../shared';
import { SessionListComponent } from './session-list.component';

describe('SessionListComponent', () => {
  let component: SessionListComponent;
  const authServiceMock: AuthService = undefined;
  const voterServiceMock: VoterService = undefined;

  beforeEach(() => {
    component = new SessionListComponent(
      authServiceMock, voterServiceMock
    );
  });

  describe('ngOnChanges', () => {
    it('should filter the sessions correctly', () => {
      component.sessions = [{
        name: 'session 1', level: 'intermediate'
      }, {
        name: 'session 2', level: 'intermediate'
      }, {
        name: 'session 3', level: 'beginner'
      }] as ISession[];
      component.filterBy = 'intermediate';
      component.sortBy = 'name';
      component.eventId = 3;

      component.ngOnChanges();

      expect(component.filteredSesson.length).toBe(2);
    });

    it('should not filter the sessions when all', () => {
      component.sessions = [{
        name: 'session 1', level: 'intermediate'
      }, {
        name: 'session 2', level: 'intermediate'
      }, {
        name: 'session 3', level: 'beginner'
      }] as ISession[];
      component.filterBy = 'all';
      component.sortBy = 'name';
      component.eventId = 3;

      component.ngOnChanges();

      expect(component.filteredSesson.length).toBe(3);
    });

    it('should sort the sessions correctly', () => {
      component.sessions = [{
        name: 'session 3', level: 'beginner'
      }, {
        name: 'session 1', level: 'intermediate'
      }, {
        name: 'session 2', level: 'intermediate'
      }] as ISession[];
      component.filterBy = 'all';
      component.sortBy = 'name';
      component.eventId = 3;

      component.ngOnChanges();

      expect(component.filteredSesson[0].name).toBe('session 1');
      expect(component.filteredSesson[1].name).toBe('session 2');
      expect(component.filteredSesson[2].name).toBe('session 3');
    });
  });
});
