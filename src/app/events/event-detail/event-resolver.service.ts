import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';

import { Observable } from 'rxjs';
import { IEvent } from '..';
import { EventService } from '../shared/event.service';

@Injectable()
export class EventResolverService implements Resolve<IEvent> {
  public constructor(private readonly service: EventService) { /**/ }

  public resolve(route: ActivatedRouteSnapshot): Observable<IEvent> {
    const id = +route.params.id;
    return this.service.getOne(id);
  }
}
