import { Component, Input } from '@angular/core';

import { IEvent } from '.';

@Component({
  selector: 'buza-event-thumbnail',
  templateUrl: './event-thumbnail.component.html',
  styles: [`
    .thumbnail { min-height: 210px; }
    .pad-left { margin-left: 10px; }
    .well div { color: #bbb; }
    .green { color: #003300 !important; }
    .bold { font-weight: bold; }
  `]
})
export class EventThumbnailComponent {
  @Input() public readonly event: IEvent;

  public get isEarlyStart(): boolean {
    return this.event.time === '8:00 am';
  }

  public getStartTimeClass() {
    return { green: this.isEarlyStart, bold: this.isEarlyStart };

    // Could also work:
    // if (isEarlyStart) { return 'bold green'; }
    // return '';

    // Could also work:
    // if (isEarlyStart) { return ['bold', 'green']; }
    // return [];
  }
}
