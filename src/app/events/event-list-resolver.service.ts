import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';

import { IEvent } from '.';
import { EventService } from './shared/event.service';

@Injectable()
export class EventListResolverService implements Resolve<IEvent[]> {
  public constructor(private readonly service: EventService) { /**/ }

  public resolve() {
    return this.service.getAll();
    // Following is not needed:
    // .subscribe();
  }
}
