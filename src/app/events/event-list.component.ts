import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IEvent } from '.';

@Component({
  templateUrl: './event-list.component.html'
})
export class EventListComponent implements OnInit {
  public events: IEvent[];

  public constructor(private readonly route: ActivatedRoute) { /**/ }

  public ngOnInit(): void {
    this.events = this.route.snapshot.data.events;

  }
}
