import { FormControl } from '@angular/forms';

// Example of function returning function (as C# lambda)
export function restrictedWords(words: string[]): (control: FormControl) => {  [key: string]: any; } {
  return (control: FormControl): { [key: string]: any } => {
    if (!words) { return null; }

    const invalidWords = words
      .map((word) => control.value.includes(word) ? word : null)
      .filter((word) => !!word);
    const isErrorFound = invalidWords && invalidWords.length > 0;
    return isErrorFound ? { restrictedWords: invalidWords.join(', ') } : null;
  };
}

// Example of validation
export function restrictedWord(control: FormControl): { [key: string]: any } {
  const hasError = control.value.includes('foo');
  return hasError ? { restrictedWords: 'foo' } : null;
}
