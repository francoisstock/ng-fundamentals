import { of } from 'rxjs';

import { ISession } from './session.model';
import { VoterService } from './voter.service';

describe('VoterService', () => {
  let voterService: VoterService;
  let httpMock;

  beforeEach(() => {
    httpMock = jasmine.createSpyObj('httpMock', ['delete', 'post']);
    voterService = new VoterService(httpMock);
  });

  describe('addVoter', () => {
    it('should add the voter to the voters list', () => {
      const session = { id: 6, voters: ['joe', 'john'] } as ISession;
      httpMock.post.and.returnValue(of(false));

      voterService.addVoter(3, session, 'karim');

      expect(session.voters.length).toBe(3);
      expect(session.voters[2]).toBe('karim');
    });

    it('should call http.post with the right URL', () => {
      const session = { id: 6, voters: ['joe', 'john'] } as ISession;
      httpMock.post.and.returnValue(of(false));

      voterService.addVoter(3, session, 'karim');

      const expectedUrl = '/api/events/3/sessions/6/voters/karim';
      const expectedOptions = jasmine.any(Object); // we don't care about this third param
      expect(httpMock.post).toHaveBeenCalledWith(expectedUrl, {}, expectedOptions);
    });
  });

  describe('deleteVoter', () => {
    it('should remove the voter from the voters list', () => {
      const session = { id: 6, voters: ['joe', 'john'] } as ISession;
      httpMock.delete.and.returnValue(of(false));

      voterService.deleteVoter(3, session, 'joe');

      expect(session.voters.length).toBe(1);
      expect(session.voters[0]).toBe('john');
    });

    it('should call http.delete with the right URL', () => {
      const session = { id: 6, voters: ['joe', 'john'] } as ISession;
      httpMock.delete.and.returnValue(of(false));

      voterService.deleteVoter(3, session, 'joe');

      const expectedUrl = '/api/events/3/sessions/6/voters/joe';
      expect(httpMock.delete).toHaveBeenCalledWith(expectedUrl);
    });
  });
});
