import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'buza-not-found',
  template: `<h1 class="error-message">404'd</h1>`,
  styles: [`
    .error-message {
      marging-top: 150px;
      font-size: 170px;
      text-align: center;
    }
  `]
})
export class NotFoundComponent { /**/ }
