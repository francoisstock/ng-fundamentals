import { Directive, ElementRef, Inject, Input, OnInit } from '@angular/core';

import { JQUERY_TOKEN } from '../../core';

@Directive({
  selector: '[buzaModalTrigger]'
})
export class ModalTriggerDirective implements OnInit {
  @Input('buzaModalTrigger') public modalId: string;
  private element: HTMLElement;

  public constructor(
    private readonly elementRef: ElementRef,
    @Inject(JQUERY_TOKEN) private readonly $: any
  ) {
    this.element = elementRef.nativeElement;
  }

  public ngOnInit(): void {
    this.element.addEventListener('click', (e) => {
      this.$(`#${this.modalId}`).modal({});
    });

  }

}
