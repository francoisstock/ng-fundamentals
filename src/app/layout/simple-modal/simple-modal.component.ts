import { Component, ElementRef, Inject, Input, ViewChild } from '@angular/core';

import { JQUERY_TOKEN } from '../../core';

@Component({
  selector: 'buza-simple-modal',
  templateUrl: './simple-modal.component.html',
  styles: [`
    .modal-body { height: 250px; overflow-y: scroll; }
  `]
})
export class SimpleModalComponent {
  @Input() public title: string;
  @Input() public elementId: string;
  @Input() public closeOnBodyClick: boolean;

  @ViewChild('modalContainer', { static: true }) public containerElement: ElementRef;

  public constructor(@Inject(JQUERY_TOKEN) private readonly $: any) { /**/ }

  public closeModal(): void {
    if (!this.closeOnBodyClick) { return; }

    const nativeElement = this.containerElement.nativeElement;
    this.$(nativeElement).modal('hide');
  }
}
