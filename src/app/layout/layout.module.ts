import { NgModule } from '@angular/core';

import { SharedModule } from '../shared';
import { NavbarComponent } from './navbar/navbar.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { ModalTriggerDirective } from './simple-modal/modal-trigger.directive';
import { SimpleModalComponent } from './simple-modal/simple-modal.component';

@NgModule({
  declarations: [
    ModalTriggerDirective,
    NavbarComponent,
    NotFoundComponent,
    SimpleModalComponent,
    NotFoundComponent
  ],
  imports: [
    SharedModule
  ],
  exports: [
    ModalTriggerDirective,
    NavbarComponent,
    SimpleModalComponent
  ]
})
export class LayoutModule { }
