import { Component, OnInit } from '@angular/core';

import { EventService, IEvent, ISession } from 'src/app/events';
import { AuthService } from 'src/app/user/auth.service';

export interface IEventLookup {
  id: number;
  name: string;
}

@Component({
  selector: 'buza-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  public searchTerm = '';
  public foundSessions: ISession[];
  public events: IEvent[];

  private static sortByName(left: IEvent, right: IEvent): number {
    return left.name.localeCompare(right.name);
  }

  public get isAuthenticated(): boolean {
    return this.authService.isAuthenticated;
  }

  public get firstName(): string {
    const user = this.authService.currentUser;
    console.log(user);
    return user && user.firstName;
  }

  public constructor(
    private readonly authService: AuthService,
    private readonly eventService: EventService
  ) { /**/ }

  public ngOnInit() {
    this.eventService.getAll()
    .subscribe((events) => {
      this.events = events.sort(NavbarComponent.sortByName);
    });
  }

  public searchSessions(searchTerm: string) {
    this.eventService.searchSessions(searchTerm).subscribe((sessions) => {
      this.foundSessions = sessions;
    });
  }
}
