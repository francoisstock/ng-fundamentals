import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Observable, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { IUser } from './user.model';

@Injectable()
export class AuthService {
  public currentUser: IUser;

  public constructor(private readonly http: HttpClient) { /**/ }

  public get isAuthenticated(): boolean {
    return !!this.currentUser;
  }

  public login(userName: string, password: string) {
    // Lowercase is required by backend 'passport' library'
    const loginInfo = { username: userName, password };
    const options = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };

    return this.http.post('/api/login', loginInfo, options)
      .pipe(tap((data) => {
        // tslint:disable-next-line: no-string-literal
        this.currentUser = data['user'] as IUser;
      }), catchError((error) => of(false)));
  }

  public update(firstName: string, lastName: string) {
    // Usage of javascript spread operator
    // (cf. https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Spread_syntax)
    this.currentUser = { ...this.currentUser, firstName, lastName };

    const options = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };
    return this.http.put(`/api/users/${this.currentUser.id}`, this.currentUser, options);

  }

  public logout() {
    this.currentUser = undefined;

    const options = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };
    return this.http.post('/api/logout', {}, options);
  }

  public checkAuthenticationStatus() {
    return this.http.get('api/currentIdentity')
      .pipe(tap((data) => {
        if (data instanceof Object) {
          this.currentUser = data as IUser;
        }
      })).subscribe();
  }
}
