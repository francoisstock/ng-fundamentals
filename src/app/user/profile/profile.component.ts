import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { IToastr, TOASTR_TOKEN } from '../../core';
import { AuthService } from '../auth.service';

const startWithLetter = '[a-zA-Z].*';

@Component({
  selector: 'buza-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  public profileForm: FormGroup;

  public constructor(
    private readonly authService: AuthService,
    private readonly router: Router,
    @Inject(TOASTR_TOKEN) private readonly toastr: IToastr
  ) { /**/ }

  public ngOnInit(): void {
    const user = this.authService.currentUser;

    const firstName = new FormControl(user.firstName, [Validators.required, Validators.pattern(startWithLetter)]);
    const lastName = new FormControl(user.lastName, Validators.required);

    this.profileForm = new FormGroup({
      firstName,
      lastName
    });
  }

  public isValid(controlName: string): boolean {
    const controls = this.profileForm.controls;
    return !controls[controlName] || controls[controlName].valid || controls[controlName].untouched;
  }

  public cancel(): void {
    this.redirect();
  }

  public save(): void {
    if (this.profileForm.invalid) { return; }

    const formValue = this.profileForm.value;
    this.authService.update(formValue.firstName, formValue.lastName)
      .subscribe(() => {
        this.toastr.success('Profile saved!');
        this.redirect();
      });
  }

  public logout() {
    this.authService.logout().subscribe(() => {
      this.redirect();
    });
  }

  private redirect(): void {
    this.router.navigate(['/events']);
  }
}
