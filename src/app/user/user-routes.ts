import { Routes } from '@angular/router';

import { LoginComponent } from './login/login.component';
import { ProfileComponent } from './profile/profile.component';

export const USER_ROUTES: Routes = [{
  path: 'profile', component: ProfileComponent
}, {
  path: 'login', component: LoginComponent
}];
