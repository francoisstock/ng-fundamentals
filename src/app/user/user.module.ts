import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SharedModule } from '../shared/shared.module';
import { LoginComponent } from './login/login.component';
import { ProfileComponent } from './profile/profile.component';
import { USER_ROUTES } from './user-routes';

@NgModule({
  declarations: [
    ProfileComponent,
    LoginComponent
  ],
  imports: [
    SharedModule,

    RouterModule.forChild(USER_ROUTES)
  ],
  providers: []
})
export class UserModule { }
