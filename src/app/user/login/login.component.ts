import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { FormGroup } from '@angular/forms';
import { AuthService } from '../auth.service';

@Component({
  selector: 'buza-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  public userName: string;
  public password: string;
  public isMouseOverSubmit = false;
  public isLoginInvalid = false;

  public constructor(
    private readonly service: AuthService,
    private readonly router: Router
  ) { /**/ }

  public login(formValues): void {
    console.log(formValues);
    this.service
      .login(formValues.userName, formValues.password)
      .subscribe((response) => {
        if (response === false) {
          this.isLoginInvalid = true;
          return;
        }

        this.navigate();
      });
  }

  public cancel(): void {
    this.navigate();
  }

  private navigate(): void {
    this.router.navigate(['/events']);
  }

  public isErrorShown(form: FormGroup, controlName: string): boolean {
    const control = form.controls[controlName];
    return control && !!control.invalid && (this.isMouseOverSubmit || !!control.touched);
  }
}
