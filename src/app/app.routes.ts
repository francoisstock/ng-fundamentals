import { Routes } from '@angular/router';

import {
    CreateEventComponent,
    CreateSessionComponent,
    EventDetailComponent,
    EventListComponent,
    EventListResolverService,
    EventResolverService
} from './events';
import { NotFoundComponent } from './layout';

export const APP_ROUTES: Routes = [
    {
        path: 'events',
        component: EventListComponent,
        resolve: { events: EventListResolverService }
    },
    {
        // Must be before /events/;id
        path: 'events/new',
        component: CreateEventComponent,
        canDeactivate: ['canDeactivateCreateEvent']
    },
    {
        // /event/42 or /event/foo are eligible
        path: 'events/:id',
        component: EventDetailComponent,
        resolve: { event: EventResolverService }
        // canActivate: [EventRouteActivatorService]
    },
    {
        path: 'events/sessions/new',
        component: CreateSessionComponent
    },
    {
        path: 'users',
        loadChildren: './user/user.module#UserModule'
    },
    {
        path: '404',
        component: NotFoundComponent
    },
    {
        path: '',
        redirectTo: '/events',
        pathMatch: 'full'
    }
];
