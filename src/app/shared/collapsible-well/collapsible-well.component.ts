import { Component, Input } from '@angular/core';

@Component({
  selector: 'buza-collapsible-well',
  templateUrl: './collapsible-well.component.html',
  styleUrls: ['./collapsible-well.component.scss']
})
export class CollapsibleWellComponent {
  @Input() public title: string;
  public isVisible = false;

  public toggleContent() {
    this.isVisible = !this.isVisible;
  }
}
