import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule, Type } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { CollapsibleWellComponent } from './collapsible-well/collapsible-well.component';

const sharedModules: Array<Type<any> | any[]> = [
  CommonModule,
  FormsModule,
  HttpClientModule,
  ReactiveFormsModule,
  RouterModule
];

@NgModule({
  declarations: [CollapsibleWellComponent],
  imports: sharedModules,
  exports: sharedModules.concat([
    CollapsibleWellComponent
  ])
})
export class SharedModule { }
