# angular-fundamentals-files
This repo was created by Jim Cooper for the Angular Fundamentals course at http://app.pluralsight.com/courses/angular-fundamentals

The original repo can be found at [github](https://github.com/jmcooper/angular-fundamentals-files)

# Run
Use two terminals and type in each one:
1. `npm run server`
2. `npm start`

The website is accessible at http://locahost:4200

# Build
Type `ng build --prod --delete-output-path --output-path dist`
The build website is accessible at http://locahost:8808
Note: the server must again be previously started by `npm run server`

# Test
Run command `ng test`
The karma dashboard is at http://localhost:9876/
